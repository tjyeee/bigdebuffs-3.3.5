# BigDebuffs for WotLK (3.3.5a - Private Server)
**`BigDebuffs is an addon that hooks the Blizzard raid frames to increase the debuff size of crowd control effects. Additionally, it replaces unit frame portraits with debuff durations when important debuffs are present.`**

<img src="https://i.imgur.com/phPWWIn.jpg" width="50%">

---

### ⚠ Note: This version is not related or affiliated with the official retail/classic addon!
### 📥 [Installation](#-installation-1)
### 📋 [Report Issue](https://gitlab.com/Tsoukie/bigdebuffs-3.3.5/-/issues)
### 💬 [FAQ](#-faq-1)
### ❤️ [Support & Credit](#%EF%B8%8F-support-credit-1)

---
### Features:
- **CompactRaidFrame:** _Full support for [CompactRaidFrame](https://gitlab.com/Tsoukie/compactraidframe-3.3.5)_
- **Anchor:** _Anchor BigDebuffs to the inner (default), left, or right of the raid frames_
- **Increase Maximum Buffs:** _Sets the maximum buffs displayed to six_
- **Scale:** _Set the scale of the various types of debuffs_
- **Warning Debuffs:** _Always show warning debuffs when BigDebuffs are displayed_
- **Unit Frames:** _Show BigDebuffs on the unit frames_
- **Profiles:** _Create custom profiles_

### Commands:
- **`/bd`** _Display config window_

<!-- blank line -->
<br>
<!-- blank line -->

# 📥 Installation

1. Download Latest Release `[.zip, .gz, ...]`:
	- <a href="https://gitlab.com/Tsoukie/bigdebuffs-3.3.5/-/releases/permalink/latest" target="_blank">`📥 BigDebuffs-3.3.5`</a>
	- <a href="https://gitlab.com/Tsoukie/classicapi/-/releases/permalink/latest" target="_blank">`📥 ClassicAPI`</a> **⚠Required**
2. Extract **both** the downloaded compressed files _(eg. Right-Click -> Extract-All)_.
3. Navigate within each extracted folder(s) looking for the following: `!!!ClassicAPI` or `BigDebuffs`.
4. Move folder(s) named `!!!ClassicAPI` or `BigDebuffs` to your `Interface\AddOns\` folder.
5. Re-launch game.

<!-- blank line -->
<br>
<!-- blank line -->


# 💬 FAQ

> Does this have nameplate crowd control?

No. It was completely removed due to limitations with the older (3.3.5a) client.

> I found a bug!

Please 📋 [report the issue](https://gitlab.com/Tsoukie/bigdebuffs-3.3.5/-/issues) with as much detail as possible.

<!-- blank line -->
<br>
<!-- blank line -->


# ❤️ Support & Credit
 
If you wish to show some support you can do so [here](https://streamlabs.com/tsoukielol/tip). Tips are completely voluntary and aren't required to download my projects, however, they are _very_ much appreciated. They allow me to devote more time to creating things I truly enjoy. 💜

<!-- blank line -->
<br>
<!-- blank line -->
  
_This_ version is modified and maintained by [Tsoukie](https://gitlab.com/Tsoukie/) and is **not** related or affiliated with any other version.

### Original AddOn
**Author:** [Jordon](https://github.com/jordonwow/) (not related/affiliated with this version)